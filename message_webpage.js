//message_webpage.js
var ewmaWeight = 0.3;
var numOfProcessors;
var tabewma;
var tabId;
var processList = new Array();

chrome.system.cpu.getInfo(function(info){ 

	console.log("Cores : " + info.numOfProcessors);
	numOfProcessors = info.numOfProcessors;

});


chrome.runtime.onConnect.addListener(function(port) {

	console.assert(port.name == "knockknock");
	port.onMessage.addListener(function(msg, sendingPort) {
		tabId = sendingPort.sender.tab.id;
		if (msg.joke == "Knock knock") {
			port.postMessage({answer: "Reply", tab: tabId});
			chrome.processes.getProcessIdForTab(tabId, function(processId){
				chrome.processes.getProcessInfo(processId, true, function(processes){
					if(processes == null) {

						alert("Process is null");

					}
					else {
						
						var cpuObject = new Object();
						cpuObject.pid = processId;
						cpuObject.actualCPU = processes[processId].cpu;
            			cpuObject.url = sendingPort.sender.tab.url;
            			cpuObject.tabId = sendingPort.sender.tab.id;
            			cpuObject.ewma = 0;
						cpuObject.osPID = processes[processId].osProcessId;
						processList.push(cpuObject);
						chrome.processes.onUpdated.addListener(receiveProcessInfo);

					}
				});
			});
		}
	});  
});


function receiveProcessInfo(processes) {
	var CPU = 0;
	var normCPU=0;
	// console.log("List size before delete: " + processList.length);
   for (index in processList) {
    if(!(processList[index].pid in processes)) {
      console.log("List size before delete: " + processList.length);
      delete processList[index];
      console.log("List size after delete: " + processList.length);
    }
    else{

			processList[index].actualCPU = processes[processList[index].pid].cpu;
    	processList[index].ewma = calcEWMA(processList[index].actualCPU, processList[index].ewma);
			console.log("PID :" + processList[index].pid + "\nCPU Usage : " + processList[index].actualCPU + "\nEWMA : " +processList[index].ewma);
    }
  }
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    			chrome.tabs.sendMessage(tabId, {greeting: processList}, function(response) {
						//Hi Guys, nothing goes here.
			});
		});
}



function calcEWMA(currentCPUUsage, prevEWMA) {
	ewma = ewmaWeight * (currentCPUUsage / numOfProcessors) + (1 - ewmaWeight) * prevEWMA;
	return ewma;
}

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}



